#!/usr/bin/env sh

# This file is mostly intended from installing Gears with a quick wget to sh pipe.
# You can probably use this without the wget part, but there isn't as much of a reason to.

normal='\e[0m'
cyan='\e[36m'



# ====== check for v ====== #

if [ -d "v" ]; then 
    echo "[$cyan SETUP $normal] V already exist"
else
    
    echo "[$cyan SETUP $normal] V not found"
    echo "[$cyan SETUP $normal] checking for required packages"
    
    # ====== check for required packages ====== #
    if dpkg-query -S "build-essential" > /dev/null 2>&1; then
        echo "[$cyan SETUP $normal] package build-essential found"
    else
        echo "[$cyan SETUP $normal] package build-essential not found, installing"
        sudo apt install "build-essential" > /dev/null 2>&1
        echo "[$cyan SETUP $normal] installed"
    fi

    if dpkg-query -S "git" > /dev/null 2>&1; then 
        echo "[$cyan SETUP $normal] package git found"
    else
        echo "[$cyan SETUP $normal] package git not found, installing"
        sudo apt install git > /dev/null 2>&1
        echo "[$cyan SETUP $normal] installed"
    fi
    # ====== check for required packages ====== #
    
    echo "[$cyan SETUP $normal] installing V"

    git clone https://github.com/vlang/v > /dev/null 2>&1
    cd v 
    make > /dev/null 2>&1
    sudo ./v symlink > /dev/null 2>&1

    echo "[$cyan SETUP $normal] installed V"
fi
# ====== check for v ====== #



# ======= set up gears ======= #

# Clone
echo "[$cyan SETUP $normal] grabbing gears"
git clone https://gitlab.com/paytonthemartian/gears tmp > /dev/null 2>&1

# Compile Gears
echo "[$cyan SETUP $normal] compiling gears"
cd tmp
v -prod .

# Finish up
echo "[$cyan SETUP $normal] finishing up"
mv tmp ../gears
cd ..
rm -rf tmp

# Init the current directory.
echo "[$cyan SETUP $normal] starting gears"
./gears --init

# ======= set up gears ======= #

echo "[$cyan SETUP $normal] done"
