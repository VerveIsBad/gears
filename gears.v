module main

import os
import log
import time
import libgears
import libgears.parser

// a usage message for --help
pub const usage = 'Usage: gears [OPTIONS...] [Cog to Run]

Options:
 -D --debug        set the logging level to debug, allowing one to read debug logs
 -i --init         init a project in the current directory
 -v --version      get the current Libgears version
 -d --deps         install dependencies defined in ./gearfile
 -w --watch FILE   watches a file for changes, and when it does change, runs the provided Cog.
 -e --experimental enables experimental features. see README\'s ##experimental-features

Cog to Run: a string providing the name of the Cog that should be executed.'

// separate arguments passed like so: -dD into -d -D
fn separate_arguments(args []string) []string {
	mut sep := []string{}

	for arg in args {
		// checks if the arg was passed like -dD
		if arg.starts_with('-') && arg.len >= 2 {
			for char in arg {
				if char == `-` {
					continue
				}

				sep << '-${char.ascii_str()}'
			}
		} else {
			sep << arg
		}
	}

	return sep
}

fn main() {
	// get the time
	now := time.now()
	now_human := now.hhmmss()

	// get arguments
	args := separate_arguments(os.args)

	// check for experimental mode
	experimental_mode := '--experimental-mode' in args || '-e' in args

	// make log folders
	os.mkdir_all('logs/gears') or {}

	// make the logger itself
	mut logger := &log.Log{}
	logger.set_level(.info)
	logger.set_full_logpath('logs/gears/gears-' + now_human.replace(':', '_') + '.log')
	logger.log_to_console_too()

	// if --debug was passed, change the logger level to debug
	if '--debug' in args || '-D' in args {
		logger.set_level(.debug)
	}

	// make a note that experimental mode is enabled
	if experimental_mode {
		logger.warn('Experimental Mode is enabled. Bugs may arise.')
	}

	// init a project
	if '--init' in args || '-i' in args {
		logger.info('--init or -i were provided. No cogs will be executed.')
		
		// check for an existing gearfile
		if os.exists('./gearfile') {
			logger.error('`gearfile` already exists in current directory.')
		} else {
			// make the gearfile with the template in libgears
			os.write_file('./gearfile', libgears.basic_gearfile) or {
				logger.fatal('Failed to write to `gearfile`. This could be for many reasons, try checking the permissions of the current directory.')
			}
		}

		// same process as the gearfile, check if a .gitignore exists
		if os.exists('./.gitignore') {
			logger.error('`.gitignore` already exists in current directory.')
		} else {
			// make the gitignore with the template in libgears
			os.write_file('./.gitignore', libgears.basic_gitignore) or {
				logger.fatal('Failed to write to `.gitignore`. This could be for many reasons, try checking the permissions of the current directory.')
			}
		}
		
		exit(0)
	}

	// version
	if '--version' in args || '-v' in args {
		logger.info('Libgears version: ' + libgears.version)
		exit(0)
	}

	// help
	if '--help' in args || '-h' in args {
		println(usage)
		exit(0)
	}

	// get cogs and packages
	mut cogs := []&libgears.Cog{}
	mut packages := map[string][]string{}
	mut cognames := []string{}
	if experimental_mode {
		cogs, packages, cognames = parser.parse_gearfile_experimental(mut logger)
	} else {
		cogs, packages, cognames = parser.parse_gearfile(mut logger)
	}

	mut cog_name := ''
	mut cog_to_run := &libgears.Cog(0)

	// print cogs and packages
	// this is only enabled in debug mode to save some execution time
	if '--debug' in args || '-D' in args {
		for cog in cogs {
			logger.debug('Cog: "$cog.name"')
		}

		for name, pkgs in packages {
			logger.debug('Package "$name" with values: $pkgs')
		}
	}

	// check for and install deps
	if '--deps' in args || '-d' in args {
		libgears.dl_deps(packages, mut logger)
	}

	// run default cog if applicable
	if '_default' in cognames {
		cog_name = '_default'
	} else if args.len < 2 {
		logger.fatal('You must provide a cog or argument to run.')
		exit(1)
	}

	// find the cog to run from args
	for index, arg in args {
		if arg.starts_with('-') || index == 0 || args[index - 1] == '-w' {
			continue
		}

		cog_name = arg
	}

	// check if the cog exists
	if cog_name !in cognames {
		logger.error('Cog "$cog_name" does not exist.')
		exit(1)
	}

	// get the &Cog from the cog_name variable
	for index, cog in cogs {
		if cog.name == cog_name {
			cog_to_run = cogs[index]
			break
		}
	}

	// watch and run file when it updates
	if '--watch' in args || '-w' in args {
		// get the file to run
		mut file_name := ''

		for index, arg in args {
			if arg == '--watch' || arg == '-w' {
				file_name = args[index + 1]
			}
		}

		// make sure the file exists
		if !os.exists(file_name) {
			logger.error('File $file_name does not exist.')
			exit(1)
		}

		logger.info('Watching $file_name with cog $cog_to_run.name')

		// start watching it
		libgears.watch(file_name, cog_to_run, mut logger)
	} else {
		logger.info('Running $cog_to_run.name')
		// run the cog normally
		cog_to_run.run(mut logger)
	}

	// get finished time
	finished_time := time.now()
	finished_time_human := finished_time.hhmmss()
	run_time := finished_time - now
	
	// log the start time, end time, and how long Gears took.
	logger.debug('Start time: $now_human ($now.unix_time())')
	logger.debug('End time: $finished_time_human ($finished_time.unix_time())')
	logger.debug('Took $run_time.str() ($run_time)')
}
