module parser

import log
import os
import libgears

// parse a gearfile
// logger is a log.Log object, from V's built-in log module.
// returns three values, a list of cogs, a map of the dependencies, and a list
// of the cog names.
pub fn parse_gearfile(mut logger log.Log) ([]&libgears.Cog, map[string][]string, []string) {
	// get the lines in the gearfile
	lines := os.read_lines('gearfile') or {
		logger.fatal("Could not read gearfile. It probably doesn't exist.")
		exit(1)
	}

	mut pkgs := {
		'vpm':  []string{}
		'vpkg': []string{}
		'sh':   []string{}
	}

	mut cogs := []&libgears.Cog{}
	mut cognames := []string{}
	mut cc_name := ''
	mut cc_cmds := []string{}

	// parse lines
	for line in lines {
		l := line.trim_space()

		if l.starts_with(':') {
			cc_name = l.trim(':{ \t')
		} else if l.starts_with('-') {
			cc_cmds << l.trim('- \t')
		} else if l == '}' {
			cogs << &libgears.Cog{
				name: cc_name
				commands: cc_cmds
			}
			cognames << cc_name
			cc_name = ''
			cc_cmds = []
		} else if l.starts_with('vpm ') {
			pkgs['vpm'] << l.replace('vpm ', '')
		} else if l.starts_with('vpkg ') {
			pkgs['vpkg'] << l.replace('vpkg ', '')
		} else if l.starts_with('sh ') {
			pkgs['sh'] << l.replace('sh ', '')
		} else if l == '' {
		} else if l.starts_with('//') {
		} else if l == '{' {
		} else {
			logger.error('Error on line: $line')
		}
	}

	return cogs, pkgs, cognames
}

// [EXPERIMENTAL PARSER]
// parse a gearfile
// logger is a log.Log object, from V's built-in log module.
// returns three values, a list of cogs, a map of the dependencies, and a list
// of the cog names.
pub fn parse_gearfile_experimental(mut logger log.Log) ([]&libgears.Cog, map[string][]string, []string) {
	// get the lines in the gearfile
	lines := os.read_lines('gearfile') or {
		logger.fatal("Could not read gearfile. It probably doesn't exist.")
		exit(1)
	}

	mut pkgs := {
		'vpm':  []string{}
		'vpkg': []string{}
		'sh':   []string{}
	}

	mut cogs := []&libgears.Cog{}
	mut cognames := []string{}
	mut cog_name := ''
	mut cog_cmds := []string{}

	// parse lines
	for line in lines {
		mut l := line.trim_space()

		// thats a lotta damage- err- variables!
		// the current identifier
		mut current_id := ''
		// if the line is a command in a cog
		mut is_command := false
		// if the line is a comment, or if the line ends with a comment
		mut is_comment := false
		// what quotes (single / double) were provided to start the string
		mut quotes := ''
		// if the line is a package / dependency
		mut is_pkg := false
		mut pkg := ''
		// if the current character is part of a cogname
		mut in_cogname := false

		if l.starts_with('vpm') || l.starts_with('vpkg') || l.starts_with('sh') {
			is_pkg = true
			// this should (hopefully) always be the vpm/vpkg/sh part
			pkg = l.split(' ')[0]
			// get rid of the vpm/vpkg/sh part for the parser
			l = l.replace('vpm ', '').replace('vpkg ', '').replace('sh ', '')
		}

		for index, c in l {
			character := c.ascii_str()

			// get next and last characters
			mut next_character := ''
			mut last_character := ''

			if index != l.len - 1 {
				next_character = l[index + 1].ascii_str()
			}

			if index != 0 {
				last_character = l[index - 1].ascii_str()
			}

			// parse characters
			if character == ':' && index == 0 {
				in_cogname = true
			} else if character == '{' {
				in_cogname = false
			} else if character == '}' {
				is_command = false
				cogs << &libgears.Cog {
					name: cog_name.trim_space().replace(':', '')
					commands: cog_cmds
				}
				cognames << cog_name.trim_space().replace(':', '')
				cog_name = ''
				cog_cmds = []
			} else if character == '-' {
				is_command = true
			} else if (character == '/' && next_character == '/') && quotes == '' {
				is_comment = true
			} else if character == '"' {
				if last_character != '\\' {
					if quotes == '"' {
						quotes = ''
					} else {
						quotes = '"'
					}
				}
			} else if character == "'" {
				if last_character != '\\' {
					if quotes == "'" {
						quotes = ''
					} else {
						quotes = "'"
					}
				}
			}
			
			// skip everything else if the current line is a comment
			if is_comment {
				continue
			}

			// append to current id
			if is_pkg || is_command {
				current_id += character
			}

			if in_cogname {
				cog_name += character
			}
		}

		if is_command {
			cog_cmds << current_id.trim_left(' -')
		}

		if is_pkg {
			pkgs[pkg] << current_id
		}
	}

	return cogs, pkgs, cognames
}
