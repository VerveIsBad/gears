# "License"

To be honest, I don't care what you do with this code. I do request that if you fork this project, that you don't make it closed-source. But I won't stop you from making it as so.

As for using this in your project, just build and include the compiled binary, you don't need to put the binary in the `.gitignore`, just include it on your repo. Similar to the `gradlew` script included in Minecraft mods.

In a nutshell, there's no license here, just be courteous. As in, I would like for you to, if you fork this, make it use this "license" as well.

> If you do decide to use this "license," then thanks!
