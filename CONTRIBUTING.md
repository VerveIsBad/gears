# Contributing

To start, thank you for (potentially) ~~doing work for me~~ contributing!

## Style Guidelines

> It is recommended to run your code through `v fmt`, that will update the code to the proper format.

Just a few side notes, programmers like to disagree on these:

- Single quotes (`'`) will always be used unless the string contains a single quote, then the string will use double quotes (`"`).
- Tabs will be used rather than spaces.

## Pull Request Format

```md
# (Pull Request Name)

(Description)

## Changes

(A basic list of changes made)

## Extra Information

(Any extra info about the PR)
```

## Issue Format

```md
# (Issue Name)

(Description)

## Platform and Specs

- OS: (OS)
- Gears Version: (./gears -v)
- V Version: (v -v)

## Gearfile

(contents of your gearfile)

## Extra Information

(Any extra info about the issue)
```
