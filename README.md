<div align='center'>

# Gears

![CI](https://gitlab.com/paytonthemartian/gears/badges/main/pipeline.svg)

</div>

---

Gears is a simple program builder (For lack of a better term) and a dependency installer.

Gears is similar to Make or Gradle, which are both programs that can manage dependencies and build/test/run your code with ease.

## Usage

### Cogs

```gearfile
:hello {
    - echo "Hello, World!"
    - v run hello.v
}
```

The above will make a new cog, called `hello` and when executed, it'll run the commands listed.

To run the cog, type `gears hello`.

### Logs and Comments

A log will be created in `logs/gears/gears-<timestamp>.log`

> The only output that is logged is text logged by Gears, stdout will NOT be logged.

You can add comments with `//`. Note that the following will result in an error:

```gearfile
:hello {
    - echo test // this is an error
    // this is fine.
}
```

### Dependencies

You can install dependencies automatically, along with running a shell command when you install deps by just adding some lines, if possible, at the beggining of your gearfile:

```v
// Install Thecodrr's Crayon module (https://github.com/thecodrr/crayon)
vpm thecodrr.crayon

// Install Thecodrr's Crayon module from Vpkg
vpkg crayon

// Again, get Thecodrr's Crayon module, but this time, from source!
sh git clone https://github.com/thecodrr/crayon && mkdir modules && mv crayon modules/
```

> I used Thecodrr's modules as examples simply because they are pretty interesting. There is also no reason to find 3 seperate modules for each example.

### Default Cog

If you want to run a cog without needing to input the cog you want to run (I.e, just running `./gears`), you can make a `:_default` cog, anything in there is executed when no other cog is provided.

### Gearfile Complete Syntax

Here you can see pretty much everything there is to the gearfile's parser. You don't *have* to read this, but it'll help nonetheless.

<details>
<summary>Complete Syntax</summary>

Wow, you are actually reading this! Congrats on your bravery!

---

#### **Comments**

A line with a comment MUST start with `//`, with no characters (Aside from spaces or tabs) preceeding it.

If you have a comment after some other code, for example, after a shell command, you'll get some funny results.

Here is an example of a "bad comment" (As explained above):

```gearfile
:hello {
    - echo "Hello, World!" // Output: Hello, World!
}
```

The above code would not run the command `echo "Hello, World!"`, but rather `echo "Hello, World!" // Output: Hello, World!`

---

#### **Cog Definitions**

When defining a cog, there are two different styles you can use, both are valid, and you can pick whichever you desire.

```gearfile
// Style 1
:hello {
    - echo "Hello, World!"
}

// Style 2
:hello_2
{
    - echo "Hello again!"
}
```

---

#### **Command Lists**

Inside a cog, the list of commands to run have some quirks. When you run `cd`, it'll change the working directory of the Gears process, so be sure to `cd` back to the root of your project at the end of every cog.

You should also avoid using `&&`, primarily because any `cd` statements after a `&&` will not change the directory.

Finally, you can use any amount of spaces or tabs you'd like before the `-`, and you should always have a space after it, but that is not required. If you really want, you can even do this:

```gearfile
:hello {
 -    echo 'Hello, World!'
}
```

---

#### **Dependencies**

When defining a dependency, you must do so at the top-level of the gearfile. You can have as many spaces before or after the `vpm`, `vpkg`, or `sh` statement. Although, the following is the recommended format:

```gearfile
vpm hello.world
vpkg helloworld
sh git clone https://gitlab.com/hello/world
```

The `vpm` dependency type is for getting a package from V's offical package manager, it's packages can be seen [here](vpm.vlang.io).

The `vpkg` type is for vpkg, and it's packages can be found [here](https://vpkg-project.github.io/registry/).

Finally, the `sh` type, this one is the most powerful, but should also be avoided if possible. The best way to write one of them is as follows:

```gearfile
sh ./gears _dep_helloworld

:_dep_helloworld {
    - git clone https://gitlab.com/hello/world
    // continue hello/world installation
}
```

To install dependencies, run `./gears -d`

Note that if you have a `_default` cog, this will be executed after dependencies have been installed.

</details>

### Command Line Arguments

There are quite a few arguments and options you can provide in the `gears` command.

Before the in-depth explainations of each and every argument, here's the output of `gears -h`

```
Usage: gears [OPTIONS...] [Cog to Run]

Options:
 -D --debug        set the logging level to debug, allowing one to read debug logs
 -i --init         init a project in the current directory
 -v --version      get the current Libgears version
 -d --deps         install dependencies defined in ./gearfile
 -w --watch FILE   watches a file for changes, and when it does change, runs the provided Cog.
 -e --experimental enables experimental features. see README's ##experimental-features

Cog to Run: a string providing the name of the Cog that should be executed.
```

Now for the in-depth explainations, you really don't need to read these, I just like to provide them for people like myself who enjoy learning each and every part of the programs you use.

> If you'd like, you can also pass single character arguments like so: `-Divdew`.

<details>
<summary>In-Depth Explaination (Collapsed)</summary>

Wow. I'm impressed! Not a lot of people decide to read this much. For reference, this is on line 185, as of the commit "Added -h, -w, -D, and updated README"

Just a heads up, there is a good bit of code used for explainations here, considering you're looking at Gears, hopefully you understand V.

---

#### -D --debug

This argument will make the `log.Log{}` that handles logging set it's logging level to `.debug`. It's default is `.info`

When this argument is passed, there will be quite a bit more output provided. Here's a side-by-side (Really top-by-bottom) comparison of with and without `-D`

Without:

```
$ ./gears test
2022-02-01 13:01:33 [INFO ] Running test
Hello, Gears!
```

With:

```
2022-02-01 13:01:50 [INFO ] Running test
Hello, Gears!
2022-02-01 13:01:50 [DEBUG] Start time: 13:01:50 (1643720510)
2022-02-01 13:01:50 [DEBUG] End time: 13:01:50 (1643720510)
2022-02-01 13:01:50 [DEBUG] Took 2.874ms (2874000)
```

If you download dependencies with `-D` passed, then even more debug information will be provided.

Cog names will also be provided when `-D` is passed, although, this is not included in the examples above, due to there being so much more output, the `./gearfile` has quite a few cogs, so the `-D` output is decently long.

#### -i --init

This argument will, when passed, make two files in the current directory, skipping their creation if they already exist.

Files to be created:

- `gearfile`

    ```gearfile
    // This file was generated by ./gears --init

    :run {
    	- v run .
    }

    :build {
    	- v -prod .
    }

    :gearup {
    	- git clone https://gitlab.com/paytonthemartian/gears tmp
    	- cd tmp
    	- v -prod .
    	- mv ./tmp ../gears
    	- cd ..
    	- rm -rf tmp
    }
    ```

- `.gitignore`

    ```gitignore
    # This file was generated by ./gears --init

    # Builds
    /main
    build/

    # Executables / Libraries
    *.so
    *.exe
    *.dll
    *.dylib

    # Logs
    /logs

    # Temporary folder
    /tmp
    ```

> This argument will prevent all Cogs from running. After the .gitignore file is made, `exit(0)` is called.

#### -v --version

Quite simply, this argument will make Gears print the version of Libgears you are running. For example, `1.0.0-beta-1`

> Libgears will always be the same version as Gears. The `version` variable is located in Libgears, hence why I used "version of Libgears" and not "version of Gears"

> Like --init, this will prevent all Cogs from running.

#### -d --deps

This argument will download all dependencies that are provided in the `gearfile`.

When `-D` is passed with `-d`, there will be more debug information provided for dependency downloads.

#### -w --watch FILE

This is probably the most complicated argument, and the hardest to explain...

`-w` will watch a file, so when it's changed (When it's mtime / modification time), the provided cog will be executed.

Pretty much, this command...:

```sh
$ ./gears hello_world -w test.v
```

...will run the `hello_world` cog when `test.v` is updated.

This is similar to V's built-in `watch` or `-live`, except, it's Gears. Simple.

#### -e --experimental

This enables experimental mode. See [##experimental-features]()

</details>

## Installation

There are three major ways to install Gears, a pre-built bianary, a quick install from source (Recommended), and building from source.

Also, there is a Visual Studio Code extension made by your's truly that adds syntax highlighting to the Gearfile. You can find it [here](https://marketplace.visualstudio.com/items?itemName=PaytonTheMartian.gears-vsc).

Finally, if you would like to view the LOC for Gears for whatever reason, it's included with every build_prod task in the CI/CD > Jobs tab.

### Pre-Built Binaries

You can find pre-built binaries on the releases tab of Gears, right [here](https://gitlab.com/PaytonTheMartian/gears/-/releases)!

Under the latest release, find the link that says "Last commit of version (some commit id)", download this file. That's the binary.

Finally, you may want to run `./gears --init` to get a simple Gearfile.

### Quick Install from Source (Recommended Method)

This install is quite fast, and you just need to run one command to get the binary *and* a basic Gearfile.

```sh
$ wget -O - https://gitlab.com/PaytonTheMartian/gears/-/raw/main/install.sh | sh
```

### Building From Source

I've tried to keep Gears simple when building from source, I think that it serves up to this goal, clone, build, mv/cp, init, done.

```sh
$ git clone https://gitlab.com/paytonthemartian/gears
$ cd gears

$ v -prod .

$ mv ./gears /path/to/target/

$ ./gears --init
```

That's it. Simple.

## Experimental Features

Gears will occasionally get new features that aren't complete, in need of optimization, or potentially buggy, these are experimental features, which by default are not enabled.

To use these, add the `-e` or the `--experimental` argument in the command line, and they'll be enabled!

### Current Experiments

- Experimental Parser (See [CHANGELOG.md](./CHANGELOG.md) version 1.0.0-beta-1)
    - Reason for being experimental: Potentially buggy

## "License"

See [this document](LICENSE.md) for the license.

## Finally

Someway, somehow, this file has reached 170+ lines... I guess that a wall of text has consumed me...

oh no... as of the commit this line is from, the readme is over 350 lines long... ;-;
