# Changelog

## 1.0.0

First official release of Gears! No more Beta!

## 1.0.0-beta-1

Starting to get somewhere!

Changes:
- Separated Gears into two parts, Gears and Libgears.
- Moved the code for parsing the Gearfile into `libgears/parser/parser.v`

Added:
- `v.mod` files
- `--init` / `-i` argument to make a basic `gearfile` and `.gitignore`
- `--watch` / `-w` argument to run a cog when a file changes!
- `--debug` / `-D` argument for making the logger log debug, such as how long Gears was running for.
	- This will log, primarily, the start and end times in `HH:MM:SS (<unix timestamp>)`
	- Along with this, the time that Gears took will be logged as `<time took> (<start timestamp - end timestamp>)`
- `--help` / `h` argument for getting a fancy usage / help message
- The option for joining single character arguments
	- Not sure what the proper term is for this. Here's an example, these examples would have the same result.
		```
		$ ./gears -d -D test

		$ ./gears -dD test
		```

Experimental Features:
- An updated parser that (should) be more flexible!
	- The parser is a *little* slower, but the difference is so INSANELY tiny that you won't even notice. There will be a new CI test with experimental mode.
	- Time differences:
		```
		These are both on the `test` cog.
		Both also do not use -prod.
		
		Normal Parser:
			Approx. 1.200ms

		Experimental Parser:
			Approx. 1.400ms
		```
	- The experimental parser also jumps all over the place with it's times, as in, one execution I would get 10ms, the next, 1.0ms. Take the time differences with a grain of salt.
	- The parser includes better comments, as in, the following is now possible:
		```
		// hello world
		:test {
			- echo "Hello, World" // Output: Hello, World!
		}
		```
	- If `//` is in a string (Any text inside double or single quotes) then it will not be seen as a comment
	- The parser can determine if it's in a string or not. This feature is currently a little buggy.

Fixed:
- Using tabs for indentation in the `gearfile` causes syntax errors.
- The requirement of a `gearfile` to exist to use `-v` and `-i`

Removed:
- The commented-out SHell script commands to install V, because it was broken.
	- This code was commented-out in a previous commit.

Known Bugs:
- ~~CI/CD fails due to `v.mod` in the `gears` folder. This is being looked into by the V developers (I think)~~
	- See [this] issue on the V GitHubrepo.
	- Fixed by moding `libgears` to `modules/libgears` and removing `/v.mod`.

Known Bugs (Experimental Features):
- ~~`sh echo 'https://hello.world.com?q=\'test\''` causes this: `sh: 1: Syntax error: Unterminated quoted string`~~
	- Fixed, along with strings just being buggy in general.
	- Fixed as of commit "Fixed strings in the experimental parser!"

## 1.0.0-beta

First release for Gears!

Contents:
- Cogs (Obviously!)
- Logging with V's built-in `logger` module.
- A `_default` cog if none are specified.
- Dependency management with VPM, Vpkg, and Sh(ell) commands (For Git, Mercurial, etc.)

Important Commits:
- [Added the program](https://gitlab.com/PaytonTheMartian/gears/-/commit/3159db4347e46259d7349cd05a3fdcd11bbf5496)
- [Better logging, and added deps!](https://gitlab.com/PaytonTheMartian/gears/-/commit/2bccd8b28aa303e9521ed2c529b9fc6ff594cde4)
- [V fmt, updates to .gitignore, and a _default cog](https://gitlab.com/PaytonTheMartian/gears/-/commit/64273223dab4b1c6c26f50f460a5f1c3325c6bd5)
- [Added Vpkg deps, updated CI/CD, and updated README](https://gitlab.com/PaytonTheMartian/gears/-/commit/994caaec704ca7b0920e4c3e3d29cb527e2a33ac)
